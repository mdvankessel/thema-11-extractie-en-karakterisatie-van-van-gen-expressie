# ReadMe Extractie en karakterisatie van gen - expressie netwerken

This repository creates from raw data from NCBI a gene expression network. A snakemake pipeline is used to clean the data and create count data. The plots in the R script shows a summary of the count data and search for correlation to build a gene expression network with the WGCNA package.

---

## Getting started

```
git clone https://Pkamphuis@bitbucket.org/mdvankessel/thema-11-extractie-en-karakterisatie-van-van-gen-expressie.git
```

---

### Prerequisites

What things you need to install the software and how to install them.

* [Trimmomatic 0.36](http://www.usadellab.org/cms/?page=trimmomatic) (use the 0.36 binary version)
* [Snakemake](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html) (Command linux: pip3 install snakemake)

---

### Repository structure
* README.md
#### data
* SraAccList.txt
##### case
* <SRR2961367.clean_feature_counts.txt - SRR2961402.clean_feature_counts.txt>
##### control
* <SRR2961330.clean_feature_counts.txt - SRR2961366.clean_feature_counts.txt>
#### images
##### Sprint 2
* ClusterDendogram.png
* ClusterForOutliers.png
* GeneSignificanceScatter.png
* Log2Boxplot.png
* OutLiers.png
* RawCountsBoxplot.png
* ScaleIndependence_MeanConnectivity.png
* TraitRelation.png
##### Sprint 3
* Control-Dendrogram[1:6].png
* NetworkMusMusculusControl.png
##### Sprint 4
* CASE-Dendrogram-Block-[1:6].png
#### pipeline
* condor.cfg
* config.yaml
* create_count_data.snakefile
* dagNewPipeline.png
* mapper.snakefile
* Snakefile
* task.sh
* trimmomatic.snakefile
#### presentations, reports & poster
##### Sprint 1
* presentation sprint 1.pdf
##### Sprint 2
* PKamphuis_MDvanKessel_presentation_sprint2.pdf
* PosterThema11.pdf
* Presentation Sprtin 2.odp
* ReportThema11.odt
##### Sprint 3
* Sprint 3.pdf
* Sprint 3.pptx
##### Sprint 4
* -
#### R
##### NetworkCreator
* Config.R
* CreateNetwork.R
* PrepareNetwork.R
* ReadMe.md
###### functions
* ExportCytoscape.R
* ExportVisAnt.R
* MainScriptRemote.R
* prepare_data.R
* prepare_network.R
##### Visulaisation
* dataExplorationThema11.Rmd
* igraph_package_graph.Rmd
* VisualisationInR.R

---

### Usage

A step by step series of examples that tell you how to get a development env running

The data is stored on the server assemblix. First go to assemblix. Second command is path to data:

```
ssh assemblix
```

path to data on assemblix:

```
/data/storix2/student/Thema11/mmu/data/PRJNA304160/
```

Before you run the pipeline, check the paths in config.yaml. Use the right paths for your own file structure.
To run the pipeline you need to go into the virtual environment:

```
source venv/bin/activate
```

Then, run snakemake. Amount of cores based on amount of threads used in the script. Use 20 cores to run the pipeline optimal:

```
snakemake --cores 20
```

This is the structure of the pipeline created with snakemake:
![Workflow overview](/pipeline/dagNewPipeline.png)

To run the R scripts, open Rstudio and use the count data from the pipeline to run the script. 

---

# Project Management
[Trello](https://trello.com/b/IkFHxU6j/extractie-en-karakterisatie-van-van-gen-expressie-netwerken)

---

# Authors
* **Maarten van Kessel** - [mdvankessel](https://bitbucket.org/mdvankessel/)
* **Priscilla Kamphuis** - [Pkamphuis](https://bitbucket.org/Pkamphuis/)

---

## License
Copyright (c) 2019 <Priscilla Kamphuis and Maarten van Kessel>.
Licensed under GPLv3. See gpl.md
