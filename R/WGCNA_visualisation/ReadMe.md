# NetworkCreator ReadMe.md

## Description
PrepareNetwork.R and CreateNetwork.R respectivly prepare the data to
create the network and then create the actual network.

Config.R is the config file where variables can be set.

PrepareNetwork.R uses the prepare_data.R and, prepare_network.R scripts.

CreateNetwork.R uses the MainScriptRemote.R, ExportVisAnt.R and,
ExportCytoscape.R.

## Prerequisites
* R
* WGCNA

## Files
functions/
    ExportCytoscape.R
    ExportVisAnt.R
    MainScriptRemote.R
    prepare_data.R
    prepare_network.R
Config.R
Prepare.R
CreateNetwork.R

## Config
### Count Data
Parameter | Description | Default
--- | --- | ---
counts_path | Path to count folder | `"/homes/mdvankessel/data/counts/case/"`
prepared_counts | Output path for prepared count data | `"/data/storix2/students/Thema11/mmu/TEST_DATA/counts_case.data"`

### Count Setup
Parameter | Description | Default
--- | --- | ---
sample_group | Grouping of the samples | `c(rep(0, 6), rep(2, 6), rep(6, 6), rep(12, 6), rep(18, 6), rep(24, 6))`

### Plots
Parameter | Description | Default
--- | --- | ---
plot_path | Path to save plots | `"/data/storix2/students/Thema11/mmu/TEST_DATA/"`
sample_clustering | Base name for sample clustering dendrogram | `"Sample_clustering"`
dendrogram_filename | Basename dendrogram plot | `"Dendrogram"`
s.i_m.c | Base name Scale independence / mean connectivity plot | `"Scale-indepen_Mean-connect_case"`

### Network
Parameter | Description | Default
--- | --- | ---
power | Power from 'Scale Independence / Mean Connectivity' plot | 6
network_path | Path to network file | `"/data/storix2/students/Thema11/mmu/TEST_DATA/"`
network_name | Base name network file | `"network_case"`

### Export Network
Parameter | Description | Default
--- | --- | ---
visant | Switch `[True / False]` | `TRUE`
module | Module name | `"brown"`
cytoscape | Switch `[True / False]` | `FALSE`
modules | Module names | `<- c("brown", "red")`

## Usage
1. Make sure variables are correct in Config.R.
2. Run PrepareNetwork.R with `Rscript PrepareNetwork.R`.
3. Edit the power value depeding on the Scale independantMean - 
   Connectivity plot, that will be opened.
4. Run CreateNetwork.R with `Rscript CreateNetwork.R`

## Citations
* [WGCNA](https://horvath.genetics.ucla.edu/html/CoexpressionNetwork/Rpackages/WGCNA/ "horvath.genetics.ucla.edu")