# igraph Visualisation ReadMe.md

## Description
This folder contains the files to filter the DGE's from the original count data and to make an igraph visualisation.

It is important to run deseq2.Rmd first, because igraph_package_graph.Rmd needs the file igraphCounts.data produced by deseq2.Rmd.

## Prerequisites
* R
* R package: DESeq2
* R package: igraph

## Files
deseq2.Rmd
igraph_package_graph.Rmd

## Usage
1. Run deseq2.Rmd
2. Check if the following files are there: igraphCounts.data & DGEs.data
3. Run igraph_package_graph.Rmd to make graphs

