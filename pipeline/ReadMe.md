Pipeline

## Installation
What things you need to install the software and how to install them.

* [Trimmomatic 0.36](http://www.usadellab.org/cms/?page=trimmomatic) (use the 0.36 binary version)
* [Snakemake](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html) (Command linux: pip3 install snakemake)

This Trimmomatic installation needs to be in the directory /Eindopdracht of this project.

## Pipeline structure

![Workflow overview](Pipeline.png)

This repository hosts the code for the final project for dataprocessing (see workflow in image above), 
and the code made as homework for 5 lessons. 
The final pipeline, which could be found in the 'Eindopdracht' folder, 
creates clean count data from rna-seq data achieved on the NCBI website.
The data used in the final pipeline could be found on the Assemblix server on the following path:

```
ssh assemblix
cd /data/storix2/student/Thema11/mmu/data/PRJNA304160/
```

## Usage

To run this pipeline after cloning, activate the virtual environment with this command:

```
source venv/bin/activate
```

The commands to run this script can also be found in the file task.sh.

After activating the virtual environment, the pipeline can be executed on assemblix with this command:

```
snakemake --cores 20
```

The tools use 40 threads, therefore the amount of cores should be 20

## Short description of every rule in the final pipeline

### Trimmomatic
Trimmomatic is used to clean the data. Every part that does not meet the requirements of
good quality data is removed by this tool. The output is a file with only clean data.

### STAR mapper
STAR is a tool to map the clean data against a reference genome.
This tool creates a file with mapped data. This tool was chosen, because it is a lot faster than hisat2 (another mapper).

### Samtools sort
Samtools sort sorts the output data from STAR so the tool FeatureCounts can use it.
Otherwise FeatureCounts does not know which parts are forward and reverse from this file.

### FeatureCounts
FeatureCounts is a tool to create count data from a sam file created above. 
Count-data is the final product we need to use further in this process.
FeatureCounts was chosen, because it uses threads, so it is faster.

### Linux cut tool
The last tool is the Linux cut tool to remove the columns we don't use. It only keeps the 
colums with the genes and the expression number. (This tool gives the same output as ht-seq count after 
running this tool).
