#map the clean data against a reference genome
#Hisat2 can use threads option after hisat2 as option -p or --threads
rule STAR_mapper:
	input:
		trim_data1 = join(wdir, output_dir, "{sample}.output_forward_paired.fastq"),
		trim_data3 = join(wdir, output_dir, "{sample}.output_reverse_paired.fq"),
		genome_tran=config["genome_tran"],
		dir = join(wdir,output_dir,"")
	output:
		join(wdir, output_dir, "{sample}.Aligned.out.sam"),
		
	message: "Align data with STAR on input {input} to generate aligned datafile {output}"
	log:
		"logs/star_mapper/{sample}_mapper.log"
	threads: 40
	shell:
		"STAR --runThreadN {threads} --genomeDir {input.genome_tran} --readFilesIn {input.trim_data1} {input.trim_data3} --outFileNamePrefix {output} 2> {log} && mv {output}Aligned.out.sam {output}"



#Sort aligned data for the count data tool
rule samtools_sort:
	input:
		join(wdir, output_dir, "{sample}.Aligned.out.sam")
	output:
		join(wdir, output_dir, "{sample}.sorted_data_test.sam")
	message: "Sorting on the following input {input} to generate sorted read files {output}"
	log:
		"logs/samtools_sort/{sample}.samtools_sort_test.log"
	threads: 40
	shell:
		"samtools sort -@ {threads} {input} -o {output} 2> {log}"
