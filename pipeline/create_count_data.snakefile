#Make count data
#Usage: usage: htseq-count [options] alignment_file gff_file
rule create_count_data:
	input:
		mapped_data = join(wdir, output_dir, "{sample}.sorted_data_test.sam"),
		ref_genome=config["Reference_genome"]
	output:
		join(wdir, output_dir, "{sample}.count_data.txt")
	log:
		"logs/feature_counts/{sample}_count_data.log"
	message: "creating count data from input {input} and create output {output} with count data"
	threads: 40
	shell:
		"featureCounts -a {input.ref_genome} -o {output} {input.mapped_data} -T {threads} 2> {log}"


rule clean_feature_counts:
	input:
		join(wdir, output_dir, "{sample}.count_data.txt")
	output:
		join(wdir, output_dir, "{sample}.clean_feature_counts.txt")
	log:
		"logs/clean_feature_counts/{sample}_clean_count_data.log"
	message: "creatin clean count data from input {input} and create output {output}"
	shell:
		"cut -f1,7,8,9,10,11,12 {input} > {output} 2> {log}"
