wdir = "/data/storix2/student/Thema11/mmu/" 
output_dir = "data"
data_dir = "data/PRJNA304160"

#Trimmomatic to create clean data. Data is paired end (PE)
#Difference Single end and paired end: TRAILING:3 
#Trimmomatic uses threads with option -threads after path to trimmomatic and PE

rule clean_data_trimmomatic:
	input:
		forward = join(wdir, data_dir, '{sample}_1.fastq.gz'),
		reverse = join(wdir, data_dir, '{sample}_2.fastq.gz')
	output:
		output1 = join(wdir, output_dir, "{sample}.output_forward_paired.fastq"),
		output2 = join(wdir, output_dir, "{sample}.output_forward_unpaired.fq"),
		output3 = join(wdir, output_dir, "{sample}.output_reverse_paired.fq"),
		output4 = join(wdir, output_dir, "{sample}.output_reverse_unpaired.fq")
	message: "Trimmatic tool to remove low quality reads on {input} and generate a fastq file with clean data {output}"
	threads: 40
	shell:
		"java -jar Trimmomatic-0.36/trimmomatic-0.36.jar PE -threads {threads} -phred33 {input.forward} {input.reverse} {output.output1} {output.output2} {output.output3} {output.output4} ILLUMINACLIP:Trimmomatic-0.36/adapters/TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36 "
